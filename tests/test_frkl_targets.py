#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `frkl_targets` package."""

import frkl.targets
import pytest  # noqa


def test_assert():

    assert frkl.targets.get_version() is not None

# -*- coding: utf-8 -*-

"""Main module."""
import collections
import logging
import os
from abc import ABCMeta, abstractmethod
from typing import Any, Dict, Iterable, Mapping, MutableMapping, Optional, Union

from frkl.common.filesystem import ensure_folder
from frkl.targets.explain import TargetExplanation, TargetItemExplanation


log = logging.getLogger("frkl_targets")


class TargetItem(metaclass=ABCMeta):
    def __init__(self, id: str, parent: "Target"):

        self._id: str = id
        self._parent: "Target" = parent

        self._metadata_cached: Optional[Mapping[str, Any]] = None

    @property
    def id(self) -> str:

        return self._id

    async def get_metadata(self) -> Mapping[str, Any]:

        if self._metadata_cached is None:
            self._metadata_cached = await self._retrieve_metadata()
        return self._metadata_cached

    @abstractmethod
    async def _retrieve_metadata(self) -> Mapping[str, Any]:
        pass

    def __eq__(self, other):

        if not isinstance(other, self.__class__):
            return False

        return self.full_path == other.full_path

    def __hash__(self):

        return hash(self.full_path)

    def __str__(self):

        return self.id

    def __repr__(self):

        return f"[{self.__class__.__name__}: id={self.id}]"


class MetadataFileItem(TargetItem):
    def __init__(self, id: str, parent: "Target", metadata: Mapping[str, Any]):

        super().__init__(id=id, parent=parent)
        self._file_path: str = os.path.abspath(os.path.expanduser(id))
        self._metadata: Mapping[str, Any] = metadata

    def get_full_path(self) -> str:
        return self._file_path

    async def get_metadata(self) -> Mapping[str, Any]:
        return self._metadata

    async def _retrieve_metadata(self) -> Mapping[str, Any]:
        pass

    @property
    def exists(self):
        return os.path.exists(self.full_path)

    @property
    def file_name(self) -> str:
        return os.path.basename(self.full_path)

    @property
    def full_path(self) -> str:
        return self._file_path

    def unlink(self):
        os.unlink(self.full_path)

    def ensure_parent_exists(self):
        ensure_folder(os.path.dirname(self.full_path))

    def explain(self) -> TargetItemExplanation:

        return TargetItemExplanation(self)


class Target(metaclass=ABCMeta):
    def __init__(self):

        self._managed_item_ids: Optional[Iterable[str]] = None
        self._managed_items: Dict[str, TargetItem] = {}

    def invalidate(self):

        self._managed_item_ids = None
        self._managed_items.clear()

    async def get_managed_item_ids(self) -> Iterable[str]:

        if self._managed_item_ids is None:
            self._managed_item_ids = await self._get_managed_item_ids()
        return self._managed_item_ids

    async def get_items(self, *item_ids: str) -> Mapping[str, Optional[TargetItem]]:

        if not item_ids:
            _item_ids: Iterable[str] = await self.get_managed_item_ids()
        else:
            _item_ids = item_ids

        to_retrieve = []
        result: Dict[str, Optional[TargetItem]] = {}
        for item in _item_ids:
            if item in self._managed_items.keys():
                result[item] = self._managed_items[item]
            else:
                to_retrieve.append(item)

        if to_retrieve:
            to_add = await self._get_items(*to_retrieve)
            result.update(to_add)

        return result

    async def get_item_metadata(self, item_id: str) -> Optional[TargetItem]:

        item = await self.get_items(item_id)
        return item[item_id]

    @abstractmethod
    async def _get_managed_item_ids(self) -> Iterable[str]:
        pass

    @abstractmethod
    async def _get_items(self, *item_ids: str) -> Mapping[str, Optional[TargetItem]]:
        pass

    async def merge_item(
        self,
        item_id: str,
        item: Any,
        item_metadata: Optional[Mapping[str, Any]] = None,
        merge_config: Optional[Mapping[str, Any]] = None,
    ) -> MutableMapping[str, Any]:

        if item_metadata is None:
            item_metadata = {}
        if merge_config is None:
            merge_config = {}

        result: Optional[Union[Mapping[str, Any], str, bool]] = await self._merge_item(
            item_id=item_id,
            item=item,
            item_metadata=item_metadata,
            merge_config=merge_config,
        )

        if result is None:
            _result: MutableMapping[str, Any] = {"merged": True}
        elif isinstance(result, bool):
            _result = {"merged": result}
        elif isinstance(result, str):
            _result = {"msg": result, "merged": True}
        elif isinstance(result, collections.abc.Mapping):
            _result = dict(result)
            if "merged" not in _result:
                _result["merged"] = True
        else:
            raise TypeError(
                f"Invalid return type '{type(result)}' for merge result: {result}"
            )

        _result["target_item"] = MetadataFileItem(
            id=item_id, parent=self, metadata=item_metadata
        )
        return _result

    @abstractmethod
    async def _merge_item(
        self,
        item_id: str,
        item: Any,
        item_metadata: Mapping[str, Any],
        merge_config: Mapping[str, Any],
    ) -> Optional[Union[Mapping[str, Any], str, bool]]:
        pass

    def explain(self) -> TargetExplanation:

        return TargetExplanation(self)

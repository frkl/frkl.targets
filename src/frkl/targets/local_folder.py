# -*- coding: utf-8 -*-
import json
import logging
import os
import shutil
from pathlib import Path
from typing import (
    TYPE_CHECKING,
    Any,
    Dict,
    Iterable,
    List,
    Mapping,
    MutableMapping,
    Optional,
    Union,
)

from anyio import create_task_group, open_file
from deepdiff import DeepHash
from frkl.common.exceptions import FrklException
from frkl.common.filesystem import ensure_folder
from frkl.explain.explanation import Explanation
from frkl.targets.defaults import (
    FRKL_MANAGED_FILES_FOLDER,
    FRKL_MANAGED_FILES_ITEMS_FOLDER_NAME,
)
from frkl.targets.target import MetadataFileItem, Target, TargetItem
from sortedcontainers import SortedDict


if TYPE_CHECKING:
    from rich.console import ConsoleOptions, Console, RenderResult

log = logging.getLogger("frtls")


class FolderMergeResult(Explanation):
    def __init__(
        self, target: "LocalFolder", metadata: Optional[Mapping[str, Any]] = None
    ):

        self._target: "LocalFolder" = target
        self._items: Dict[str, MutableMapping[str, Any]] = {}
        if metadata:
            metadata = dict(metadata)
        else:
            metadata = {}
        self._metadata: Dict[str, Any] = metadata

        super().__init__()

    def add_merge_item(self, id: str, **result_metadata: Any) -> None:

        # TODO: check if already exists?
        self._items[id] = dict(result_metadata)
        if "merged" not in self._items[id].keys():
            self._items[id]["merged"] = True
        if "msg" not in self._items[id].keys():
            self._items[id]["msg"] = "merged"

    def add_merge_result(self, result: "FolderMergeResult") -> None:

        self._items.update(result.merged_items)

    async def create_explanation_data(self) -> Mapping[str, Any]:

        return self.merged_items

    def add_metadata(self, key: str, value: Any) -> None:
        self._metadata[key] = value

    @property
    def merged_items(self):
        return self._items

    @property
    def metadata(self) -> Mapping[str, Any]:
        return self._metadata

    @property
    def target(self) -> "LocalFolder":
        return self._target

    def __rich_console__(
        self, console: "Console", options: "ConsoleOptions"
    ) -> "RenderResult":

        if not self.merged_items:
            yield f"- no items merged into folder '[italic]{self.target.path}[/italic]'"
            return

        yield f"- merged items into '[italic]{self.target.path}[/italic]':"
        yield ""

        for id, details in self.merged_items.items():
            yield f"  [key2]{id}[/key2]: [value]{details['msg']}[/value]"

        return


class LocalFolder(Target):
    def __init__(self, path: Optional[Union[str, Path]] = None):

        if isinstance(path, Path):
            path = path.as_posix()

        if not path:
            path = os.getcwd()

        self._path: str = os.path.abspath(os.path.expanduser(path))
        super().__init__()

    @property
    def path(self) -> str:
        return self._path

    @property
    def base_name(self):

        return os.path.basename(self.path)

    def ensure_exists(self) -> None:

        ensure_folder(self._path)

    def exists(self, rel_path: Optional[str] = None):

        path = self.get_full_path(rel_path)
        return os.path.exists(path)

    def get_full_path(self, rel_path: Optional[str] = None):

        if not rel_path:
            return self._path

        path = os.path.join(self.path, rel_path)
        return path

    async def merge_folders(
        self,
        *source_folders: Union[str, Path],
        item_metadata: Optional[Mapping[str, Any]] = None,
        merge_config: Optional[Mapping[str, Any]] = None,
    ) -> FolderMergeResult:

        if item_metadata is None:
            item_metadata = {}

        if merge_config is None:
            merge_config = {}

        copy_dict: Dict[str, str] = {}
        for source_folder in source_folders:
            cd = self._prepare_folder_merge(
                source_folder=source_folder, merge_config=merge_config
            )

            for target in cd.values():

                if target in copy_dict.values():
                    raise FrklException(
                        msg=f"Can't merge folders: {os.path.basename(source_folder)} -> {self.base_name}",
                        reason=f"Duplicate target file: {target}",
                    )
            copy_dict.update(cd)

        result = FolderMergeResult(target=self)

        # exist: List[str] = []
        # no_exist: List[str] = []
        # for k, v in copy_dict.items():
        #     p = os.path.join(self.path, v)
        #     if os.path.exists(p):
        #         exist.append(v)
        #     else:
        #         no_exist.append(v)
        #
        # if exist:
        #     for f in exist:
        #         result.add_merge_item(f, msg="exists, doing nothing")
        #     for f in no_exist:
        #         result.add_merge_item(f, msg="does not exist, ignoring")
        #
        #     return result

        # result: Dict[str, Mapping[str, Any]] = {}
        for source, target in copy_dict.items():

            p = os.path.join(self.path, target)
            exists: bool = os.path.exists(p)

            if exists and not merge_config.get("force", False):
                merge_result: MutableMapping[str, Any] = {
                    "merged": False,
                    "msg": "file exists, not merged",
                }
            else:
                merge_result = await self.merge_item(
                    item_id=target,
                    item=source,
                    item_metadata=item_metadata,
                    merge_config=merge_config,
                )
            result.add_merge_item(target, **merge_result)

        return result

    def _prepare_folder_merge(
        self, source_folder: Union[str, Path], merge_config: Mapping[str, Any]
    ) -> Mapping[str, str]:
        """Create dict with all source/target paths."""

        flatten: bool = merge_config.get("flatten", False)
        exclude_dirs = merge_config.get("exclude_dirs", [])

        if isinstance(source_folder, Path):
            source_folder = source_folder.as_posix()

        copy_dict: Dict[str, str] = {}

        for root, dirnames, filenames in os.walk(source_folder, topdown=True):

            if exclude_dirs:
                dirnames[:] = [d for d in dirnames if d not in exclude_dirs]

            for filename in filenames:

                source_file_item = os.path.join(root, filename)
                rel_path = os.path.relpath(source_file_item, source_folder)

                if flatten:
                    target_file = filename
                else:
                    target_file = rel_path

                if target_file in copy_dict.values():
                    raise FrklException(
                        msg=f"Can't merge folder: {os.path.basename(source_folder)} -> {self.base_name}",
                        reason=f"Duplicate target file: {target_file}",
                    )

                copy_dict[source_file_item] = target_file

        return copy_dict


class TrackingLocalFolder(LocalFolder):
    def __init__(self, path: Union[str, Path]):

        super().__init__(path=path)

        self._metadata_folder: str = FRKL_MANAGED_FILES_FOLDER
        self._metadata_file: str = os.path.join(
            self._metadata_folder, FRKL_MANAGED_FILES_ITEMS_FOLDER_NAME
        )
        self._item_metadata_path: str = os.path.join(
            self._metadata_folder, FRKL_MANAGED_FILES_ITEMS_FOLDER_NAME
        )
        self._item_path_hashes: str = os.path.join(self._item_metadata_path, "hashes")
        self._item_path_files: str = os.path.join(self._item_metadata_path, "files")

        self._hash_contents: Dict[str, Mapping[str, Any]] = {}

        self._metadata: Optional[Mapping[str, Any]] = None
        self._all_files_queried: bool = False
        self._all_managed_files: Dict[str, str] = {}

    async def _merge_item(
        self,
        item_id: str,
        item: Any,
        item_metadata: Mapping[str, Any],
        merge_config: Mapping[str, Any],
    ) -> Optional[MutableMapping[str, Any]]:

        if isinstance(item, Path):
            _src: str = item.as_posix()
        elif not isinstance(item, str):
            raise TypeError(
                f"Can't add item to folder: invalid item type for item '{item_id}': {type(item)}"
            )
        else:
            _src = item

        if not os.path.exists(_src):
            raise ValueError(
                f"Can't add file '{_src}' to target folder '{self.path}': file does not exist."
            )

        _sorted_item_metadata = SortedDict(item_metadata)

        if _sorted_item_metadata.get("target", None):
            raise FrklException(
                msg=f"Can't merge item '{item_id}' into target '{self.path}'.",
                reason=f"Provided metadata includes key 'target'. This is not allowed: {_sorted_item_metadata}",
            )

        _sorted_item_metadata["target"] = self.path

        hash_dict = {}
        for k, v in _sorted_item_metadata.items():
            if k.startswith("_"):
                continue
            hash_dict[k] = v

        existing_item: Optional[TargetItem] = await self.get_item_metadata(item_id)

        if existing_item is not None and not merge_config.get("force", False):
            raise Exception(f"File (incl. metadata) already exists: {item_id}")

        result = {}
        # also check if a non-metadata'ed file exists...
        full_path = self.get_full_path(item_id)
        if os.path.exists(full_path):
            if not merge_config.get("force", False):
                raise FrklException(
                    msg=f"Can't merge file {item_id}.",
                    reason=f"File already exists: {full_path}",
                )

            result["msg"] = "existing file replaced"
            if merge_config.get("backup_existing_files", True):
                print("BACKUP")

        if merge_config.get("write_metadata", False):
            hash_file = await self._ensure_metadata_file(_sorted_item_metadata)
            self._link_metadata_file(item_id=item_id, hash_file=hash_file, force=True)

        target = os.path.join(self.path, item_id)
        ensure_folder(os.path.dirname(target))

        if merge_config.get("move_method", None) == "move":
            shutil.move(_src, target)
        else:
            shutil.copy2(_src, target)

        self._all_files_queried = False

        return result

    def _get_hash_for_metadata(self, metadata: Mapping[str, Any]) -> str:

        _hashes = DeepHash(metadata)
        hash_for_dict = _hashes[metadata]

        return hash_for_dict

    async def _ensure_metadata_file(self, metadata: Mapping[str, Any]) -> str:

        hash_for_dict = self._get_hash_for_metadata(metadata)
        hash_file = os.path.join(self._item_path_hashes, f"{hash_for_dict}.json")

        if os.path.isfile(hash_file):
            return hash_file

        md_string = json.dumps(metadata)
        ensure_folder(os.path.dirname(hash_file))

        async with await open_file(hash_file, "w") as f:
            await f.write(md_string)

        return hash_file

    def _link_metadata_file(self, item_id: str, hash_file: str, force: bool = False):

        metadata_file = self._get_metadata_path_for_item(item_id)

        if os.path.islink(metadata_file):
            if force:
                os.unlink(metadata_file)
            else:
                raise FrklException(
                    f"Can't link metadata and hash files, metadata file already exists: {metadata_file}"
                )
        else:
            ensure_folder(os.path.dirname(metadata_file))

        rel_path = os.path.relpath(hash_file, os.path.dirname(metadata_file))
        os.symlink(rel_path, metadata_file)

    async def _get_managed_item_ids(self) -> Iterable[str]:

        file_map = await self._get_all_managed_file_hashes()
        return file_map.keys()

    async def _get_items(self, *item_ids: str) -> Mapping[str, Optional[TargetItem]]:

        hash_map: Dict[str, List[str]] = {}
        hash_map_reverse: Dict[str, str] = {}

        not_managed = []

        for item_id in item_ids:
            item_hash = await self._get_managed_file_hash(item_id)

            if item_hash is None:
                not_managed.append(item_id)
            else:

                hash_map_reverse[item_id] = item_hash
                hash_map.setdefault(item_hash, []).append(item_id)

        all_metadata_per_hash = {}

        async def get_md(_hash: str):
            _md = await self._get_metadata_for_hash(_hash)
            all_metadata_per_hash[_hash] = _md

        async with create_task_group() as tg:

            for hash_str in hash_map.keys():
                if hash_str in self._hash_contents.keys():
                    all_metadata_per_hash[hash_str] = self._hash_contents[hash_str]
                else:
                    await tg.spawn(get_md, hash_str)

        result: Dict[str, Optional[TargetItem]] = {}
        for _item_id, _hash in hash_map_reverse.items():

            metadata = all_metadata_per_hash[_hash]

            item: TargetItem = MetadataFileItem(
                id=_item_id, parent=self, metadata=metadata
            )
            result[_item_id] = item

        for nm in not_managed:
            result[nm] = None

        return result

    async def _get_managed_file_hash(self, item_id: str) -> Optional[str]:

        if item_id in self._all_managed_files.keys():
            return self._all_managed_files[item_id]

        full_path = self._get_metadata_path_for_item(item_id)
        if not os.path.exists(full_path):
            return None

        hash_file = os.path.realpath(full_path)
        if not os.path.exists(hash_file):
            os.unlink(full_path)
            return None

        temp = full_path[len(self._item_path_files) + 1 :]  # noqa
        index_sep = temp.index(os.path.sep)
        real_file_path = temp[index_sep:]
        link_target = os.path.join(self._item_path_files, full_path)

        if not os.path.exists(real_file_path):
            log.debug(
                f"Managed file does not exist anymore, removing link: {real_file_path}"
            )
            os.unlink(full_path)
            return None

        if not self.exists(link_target):
            log.debug(
                f"Link target '{link_target}' for file does not exist anymore, removing link: {full_path}"
            )
            os.unlink(full_path)
            return None

        result_hash = os.path.basename(hash_file)[0:-5]

        self._all_managed_files[item_id] = result_hash
        return result_hash

    async def _get_all_managed_file_hashes(self) -> Mapping[str, Any]:

        if self._all_files_queried:
            return self._all_managed_files

        all: Dict[str, str] = {}

        start_dir = os.path.join(
            f"{self._item_path_files}{os.path.sep}localhost{self.path}"
        )

        for root, dirnames, filenames in os.walk(start_dir, topdown=True):

            for filename in filenames:

                path = os.path.join(root, filename)
                item_id = os.path.relpath(path, start_dir)

                hash_str = await self._get_managed_file_hash(item_id)
                if hash_str is None:
                    continue

                all[item_id] = hash_str

        self._all_managed_files.clear()
        self._all_managed_files.update(all)
        self._all_files_queried = True
        return self._all_managed_files

    async def _get_metadata_for_hash(self, hash: str) -> Mapping[str, Any]:

        if hash in self._hash_contents.keys():
            return self._hash_contents[hash]

        hash_file = os.path.join(self._item_path_hashes, f"{hash}.json")

        async with await open_file(hash_file) as f:
            content = await f.read()

        self._hash_contents[hash] = json.loads(content)
        return self._hash_contents[hash]

    def _get_metadata_path_for_item(self, item: str) -> str:

        metadata_path = os.path.join(
            self._item_metadata_path, "files", f"localhost{self.get_full_path(item)}"
        )
        return metadata_path

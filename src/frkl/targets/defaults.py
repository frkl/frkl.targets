# -*- coding: utf-8 -*-
import os
import sys

from appdirs import AppDirs
from frkl.common.defaults import FRKL_DEFAULT_SHARE_DIR


frkl_targets_app_dirs = AppDirs("frkl_targets", "frkl")

if not hasattr(sys, "frozen"):
    FRKL_TARGETS_MODULE_BASE_FOLDER = os.path.dirname(__file__)
    """Marker to indicate the base folder for the `frkl_targets` module."""
else:
    FRKL_TARGETS_MODULE_BASE_FOLDER = os.path.join(
        sys._MEIPASS, "frkl_targets"  # type: ignore
    )
    """Marker to indicate the base folder for the `frkl_targets` module."""

FRKL_TARGETS_RESOURCES_FOLDER = os.path.join(
    FRKL_TARGETS_MODULE_BASE_FOLDER, "resources"
)
FRKL_MANAGED_FILES_FOLDER = os.path.join(FRKL_DEFAULT_SHARE_DIR, "managed_files")
FRKL_MANAGED_FILES_ITEMS_FOLDER_NAME = "items"

# -*- coding: utf-8 -*-
from typing import Any, Dict


project_name = "frkl.targets"
project_main_module = "frkl.targets"
project_slug = "frkl_targets"

pyinstaller: Dict[str, Any] = {}

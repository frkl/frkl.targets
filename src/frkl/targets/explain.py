# -*- coding: utf-8 -*-
from typing import TYPE_CHECKING, Any, Mapping

from frkl.explain.explanation import DataExplanation


if TYPE_CHECKING:
    from frkl.targets.target import TargetItem, Target


class TargetItemExplanation(DataExplanation):
    def __init__(self, data: "TargetItem"):

        super().__init__(data=data)

    async def create_explanation_data(self) -> Mapping[str, Any]:

        result = {}
        result["id"] = self.data.id
        result["metadata"] = await self.data.get_metadata()

        return result


class TargetExplanation(DataExplanation):
    def __init__(self, data: "Target"):

        super().__init__(data=data)

    async def create_explanation_data(self) -> Mapping[str, Any]:

        items = await self.data.get_items()

        return {"managed_items": items.values()}
